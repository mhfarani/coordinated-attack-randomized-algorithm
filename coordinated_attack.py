import copy
import random
from typing import List


class RandomizedCoordinatedAttack:
    new_node_state: List

    def __init__(self, node_number: int, round_number: int, failure_percent: float = 0):

        self.node_number = node_number
        self.round_number = round_number
        self.failure_percent = failure_percent

        self.decision = []
        self.node_state = [[[], []] for i in range(self.node_number)]

        self.set_init_information_level()
        self.set_init_decision()

        self.k_level = int(random.uniform(0, self.round_number))

        self.run_r_round()
        # print(self.node_state)

    def run_r_round(self):
        for r in range(self.round_number):
            self.announce_state_in_all_network_with_failure()

    def create_decision(self):
        for i in range(self.node_number):
            if self.node_state[i][0][i] >= self.k_level and not (0 in self.node_state[i][1]):
                self.decision.append(1)
            else:
                self.decision.append(0)

        decide = [i[-1] for i in self.node_state]
        number_of_commit = self.decision.count(1)
        number_of_abort = self.decision.count(0)
        # print(f' number of commit is {number_of_commit}')

        return max(number_of_abort / self.node_number, number_of_commit / self.node_number)

    def announce_state_in_all_network_with_failure(self):
        self.new_node_state = copy.deepcopy(self.node_state)

        for sender_index in range(self.node_number):
            for receiver_index in range(self.node_number):
                if random.random() > self.failure_percent:
                    self.announce_state(sender_index, receiver_index)

        self.node_state = copy.deepcopy(self.new_node_state)
        # print(self.node_state)

    def set_init_information_level(self):
        for i in range(self.node_number):
            for j in range(self.node_number):
                self.node_state[i][0].append(0) if i == j else self.node_state[i][0].append(-1)
        # print(self.node_state)

    def set_init_decision(self):
        for i in range(self.node_number):
            for j in range(self.node_number):
                self.node_state[i][1].append(random.choice([0, 1])) if i == j else self.node_state[i][1].append(2)

    def announce_state(self, sender_index: int, receiver_index: int):
        sender_state = self.node_state[sender_index]
        receiver_state = self.new_node_state[receiver_index]
        old = receiver_state[:]
        for i in range(len(receiver_state[0])):
            self.new_node_state[receiver_index][0][i] = max(receiver_state[0][i], sender_state[0][i])
        self.new_node_state[receiver_index][0][receiver_index] = min(self.new_node_state[receiver_index][0]) + 1
        self.new_node_state[receiver_index][1] = [x and y for x, y in zip(
            self.new_node_state[receiver_index][1], sender_state[1]
        )
                                                  ]
        # print('new node state')
        # print(self.new_node_state)


if __name__ == '__main__':

    for failure in [.7, .8, .9]:

        for round_number in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]:
            result = 0
            for i in range(1000):
                result += RandomizedCoordinatedAttack(10, round_number, failure).create_decision()
            print(f'percent of agreement for {round_number} r and {failure} f  is {result / 1000}')
    RandomizedCoordinatedAttack(3, 4, .9)